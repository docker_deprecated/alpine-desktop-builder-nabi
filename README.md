# alpine-desktop-builder-nabi

#### [alpine-x64-desktop-builder-nabi](https://hub.docker.com/r/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64build/alpine-x64-desktop-builder-nabi)
#### [alpine-aarch64-desktop-builder-nabi](https://hub.docker.com/r/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64build/alpine-aarch64-desktop-builder-nabi)
#### [alpine-armhf-desktop-builder-nabi](https://hub.docker.com/r/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhfbuild/alpine-armhf-desktop-builder-nabi)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [Nabi](https://github.com/libhangul/nabi/wiki/)  (Build Image)
    - The Easy Hangul XIM 



----------------------------------------
#### Run

* Nothing



----------------------------------------
#### Usage

* Nothing



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

